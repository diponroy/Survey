﻿function ComboBoxQsnVm(callbacks) {
    var self = this;
    self.questionType = _questionTypes.comboBox;
    self.guidId = ko.observable();
    self.title = ko.observable('');
    self.mark = ko.observable('');
    self.duration = ko.observable('');
    self.options = ko.observableArray([]);

    self.errors = ko.validatedObservable([self.title]);
    self.hasError = function () {

    };
    self.showError = function () {

    };
    self.removeError = function () {

    };

    self.addDefaultOption = function (data) {
        var arr = self.options();
        arr.push({ id: null, title: 'option', isCurrect: ko.observable(false) });
        self.options([]);
        self.options(arr);
    };
    self.removeOption = function (item) {
        self.options.remove(item);
    };

    self.validateQsnType = function (data) {
        if (self.questionType !== data.questionType) {
            throw error("question is not same");
        }
    };
    
    self.setToVm = function (data) {
        self.validateQsnType(data);
        self.guidId(data.guidId);
        self.title(data.title);
        self.mark(data.mark);
        self.duration(data.duration);
        self.options(data.options);
    };
    
    self.updateFromVm = function () {      
        var question = {
            guidId: self.guidId(),
            questionType: self.questionType,
            title: self.title(),
            mark: self.mark(),
            duration: self.duration(),
            options: self.options()
        };
        if (typeof callbacks.update !== "undefined" || callbacks.update == null ) {
            callbacks.update(question);
        }
    };
    
    self.addDefault = function() {
        if (!$.IsUndefinedOrNull(callbacks.add)) {
            var question = {
                guidId: '',
                questionType: self.questionType,
                title: 'ComboBox choice question title ?',
                mark: 0,
                duration: 0,
                options: [
                    { id: null, title: 'option 1', value: 'option_1', isCurrect: ko.observable(true) },
                    { id: null, title: 'option 2', value: 'option_2', isCurrect: ko.observable(false) },
                    { id: null, title: 'option 3', value: 'option_3', isCurrect: ko.observable(false) },
                    { id: null, title: 'option 4', value: 'option_4', isCurrect: ko.observable(false) }
                ]
            };
            callbacks.add(question);
        }
    };
    self.init = function() {
    };
}