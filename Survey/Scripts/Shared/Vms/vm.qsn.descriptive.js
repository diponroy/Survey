﻿function DescriptiveQsnVm(callbacks) {
    //var self = this;
    //self.questionType = _questionTypes.descriptive;
    //self.guidId = ko.observable();
    //self.title = ko.observable('');

    //self.errors = ko.validatedObservable([self.title]);
    //self.hasError = function () {

    //};
    //self.showError = function () {

    //};
    //self.removeError = function () {

    //};

    //self.validateQsnType = function (data) {
    //    if (self.questionType() !== data.questionType) {
    //        throw error("question is not same");
    //    }
    //};
    //self.show = function (data) {
    //};
    //self.add = function (data) {
    //};
    //self.addDefault = function () {
    //    if (typeof callbacks.add !== "undefined" || callbacks.add != null) {
    //        var question = {
    //            questionType: _questionTypes.descriptive,
    //            title: 'Descriptive question title ?'
    //        };
    //        callbacks.add(question);
    //    }
    //};
    //self.update = function (data) {
    //};
    
    var self = this;
    self.questionType = _questionTypes.descriptive;
    self.guidId = ko.observable();
    self.title = ko.observable('');
    self.mark = ko.observable('');
    self.duration = ko.observable('');
    self.options = ko.observableArray([]);

    self.errors = ko.validatedObservable([self.title]);
    self.hasError = function () {

    };
    self.showError = function () {

    };
    self.removeError = function () {

    };

    self.addDefaultOption = function (data) {
        var arr = self.options();
        arr.push({ id: null, title: 'option', isCurrect: ko.observable(false) });
        self.options([]);
        self.options(arr);
    };
    self.removeOption = function (item) {
        self.options.remove(item);
    };

    self.validateQsnType = function (data) {
        if (self.questionType !== data.questionType) {
            throw error("question is not same");
        }
    };

    self.setToVm = function (data) {
        self.validateQsnType(data);
        self.guidId(data.guidId);
        self.title(data.title);
        self.mark(data.mark);
        self.duration(data.duration);
        self.options(data.options);
    };

    self.updateFromVm = function () {
        var question = {
            guidId: self.guidId(),
            questionType: self.questionType,
            title: self.title(),
            mark: self.mark(),
            duration: self.duration(),
            options: self.options()
        };
        if (typeof callbacks.update !== "undefined" || callbacks.update == null) {
            callbacks.update(question);
        }
    };

    self.addDefault = function () {
        if (typeof callbacks.add !== "undefined" || callbacks.add != null) {
            var question = {
                guidId: '',
                questionType: _questionTypes.descriptive,
                title: 'Descriptive question title ?',
                mark: 0,
                duration: 0
        };
            callbacks.add(question);
        }
    };

    self.init = function () {
    };
}