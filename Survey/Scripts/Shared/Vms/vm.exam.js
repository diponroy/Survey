﻿function ExamVm() {
    var self = this;
    self.title = ko.observable('').extend({ required: true, maxLength: 200 });
    self.fromDate = ko.observable('');
    self.toDate = ko.observable('');
    
    self.errors = ko.validation.group([self.title]);
    self.hasError = function() {
        return self.errors().length > 0;
    };
    self.showError = function() {
        self.errors.showAllMessages();
    };
    self.removeError = function() {
        self.errors.showAllMessages(false);
    };
}