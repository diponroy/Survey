﻿/*question types view model*/
function QuestionTypeVm() {
    var self = this;
    self.types = ko.observableArray([]);

    self.IsValidQsnType = function (qsnType) {
        var isValid = false;
        var qsnTypes = self.types();
        if (qsnTypes.length < 1) {
            return isValid;
        }
        for (var i = 0; i < qsnTypes.length; i++) {
            if (qsnType === qsnTypes[i].type) {
                isValid = true;
                break;
            }
        }
        return isValid;
    };
    
    self.init = function () {
        var types = [
            { type: _questionTypes.singleChoice, nameText: "Single Choice", iconSyle: "icon-ok-circle" },
            { type: _questionTypes.multipleChoice, nameText: "Multiple Choice", iconSyle: "icon-check" },
            { type: _questionTypes.descriptive, nameText: "Descriptive", iconSyle: "icon-edit" },
            { type: _questionTypes.comboBox, nameText: "Combo Box", iconSyle: "icon-arrow-down" }
        ];
        self.types(types);
    };
}