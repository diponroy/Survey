﻿/*appendContent for knockout*/
ko.bindingHandlers.appendContent = {
    update: function (element, valueAccessor) {
        var value = ko.utils.unwrapObservable(valueAccessor());
        $(element).append(value);
    }
};

/*template setup*/
$(function () {
    infuser.defaults.templateUrl = "/Templates";
    infuser.defaults.templatePrefix = "_";
    infuser.defaults.templateSuffix = ".tmpl.html";
});

/*checks object has the attr*/
$.fn.hasAttr = function (attrName) {
    return this.attr(attrName) !== undefined;
};

/*checks object has the attr*/
$.IsUndefinedOrNull = function (value) {
    return typeof value === "undefined" || value == null;
};


/*Utility to clone objects prototypes*/
function prototypeClone(baseObject) {
    function baseClone() { }
    baseClone.prototype = baseObject.prototype;
    return new baseClone();
}

/*Question types*/
var _questionTypes = {
    singleChoice: "SingleChoice",
    multipleChoice: "MultipleChoice",
    descriptive: "Descriptive",
    comboBox: "ComboBox"
};
Object.freeze(_questionTypes);



/*Guid generatior
http://stackoverflow.com/questions/105034/how-to-create-a-guid-uuid-in-javascript
*/
function Guid() {
    function _p8(s) {
        var p = (Math.random().toString(16) + "000000000").substr(2, 8);
        return s ? "-" + p.substr(0, 4) + "-" + p.substr(4, 4) : p;
    }
    return _p8() + _p8(true) + _p8(true) + _p8();
}

/*Guid in obsserable
http://www.wiredprairie.us/blog/index.php/archives/1565
*/
ko.observableArray.fn.withIndex = function (keyName) {
    var index = ko.computed(function () {
        var list = this() || [];    // the internal array
        var keys = {};              // a place for key/value
        ko.utils.arrayForEach(list, function (v) {
            if (keyName) {          // if there is a key
                keys[v[keyName]] = v;    // use it
            } else {
                keys[v] = v;
            }
        });
        return keys;
    }, this);

    // also add a handy add on function to find
    // by key ... uses a closure to access the 
    // index function created above
    this.findByKey = function (key) {
        return index()[key];
    };
    
    this.hasKey = function (key) {
        return key in index();
    };
    
    return this;
};


function QuestionOption(data) {
    
}