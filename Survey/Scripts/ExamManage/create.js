﻿/*main view model*/
function ViewModel() {
    var self = this;
    self.questionTypes = new QuestionTypeVm();
    self.question = ko.observable();
    self.questions = ko.observableArray([]).withIndex("guidId");


    /*bool: checks if any question at self.question arry*/
    self.hasAnyQuestion = ko.computed(function() {
        var hasQuestion = self.questions().length > 0;
        return hasQuestion;
    });

    /*set guid id in question and returns it*/
    self.setGuidId = function(data) {
        while (true) {
            var guid = Guid();
            if (!self.questions.hasKey(guid)) {
                data.guidId = guid;
                break;
            }
        }
        return data;
    };
    
    /*returns Vm depending on the question type*/
    self.getQuestionVm = function (questionType) {
        var vm = null;
        var callBacks = { add: self.addQuestion, update: self.editQuestion };
        
        switch (questionType) {
            case _questionTypes.singleChoice:
                vm = new SingleChoiceQsnVm(callBacks);
            break;
            case _questionTypes.multipleChoice:
                vm = new MultipleChoiceQsnVm(callBacks);
                break;
            case _questionTypes.descriptive:
                vm = new DescriptiveQsnVm(callBacks);
                break;
            case _questionTypes.comboBox:
                vm = new ComboBoxQsnVm(callBacks);
                break;
            default:
                throw Error("No viewmodel found.");
        }
        return vm;
    };

    /*Add question*/
    self.addQuestion = function(data) {
        var arr = [];
        arr.push(self.setGuidId(data));
        self.questions(arr.concat(self.questions()));
        $("#divQuestions").find("> div.divQuestion:first").hide().fadeIn(1000);
    };
    self.showQsnToAdd = function (qsnType) {
        if (!self.questionTypes.IsValidQsnType(qsnType)) {
            return;
        }
        self.question(self.getQuestionVm(qsnType));
        self.question().addDefault();
    };
    
    /*delete question*/
    self.deleteQuestion = function (item) {
        self.questions.remove(item);
    };
    self.confirmQsnToDelete = function (item) {
        $("#divQuestions").find("> div.divQuestion[id='" + item.guidId + "']:first").fadeOut(500);
        setTimeout(function () {
            self.deleteQuestion(item);
        }, 600);
    };
    
    /*edit question*/
    self.editQuestion = function (data) {
        var arr = self.questions().map(function (item) { return item.guidId == data.guidId ? data : item; });
        self.questions(arr);
        $('#divQsnEditorPopup').modal('hide');
        $("#divQuestions").find("> div.divQuestion[id='" + data.guidId + "']:first").hide().fadeIn(1500);
    };
    self.showQsnToEdit = function (item) {
        self.question(self.getQuestionVm(item.questionType));
        self.question().setToVm(item);
        $('#divQsnEditorPopup').removeData("modal").modal({
            backdrop: 'static',
            keyboard: false
        });
    };
    
    /*preview exam*/
    self.previewExam = function () {
        var json = ko.mapping.toJSON(self);
        amplify.store("exam", json);
        window.open('/ExamManage/Preview', '_blank');
    };
    
    self.init = function() {
        self.questionTypes.init();
    };
}


$(document).ready(function() {
    var vm = new ViewModel();
    vm.init();
    ko.applyBindings(vm);
    ko.validatedObservable(vm);
    
    /*enabel drag*/
    $("#divQuestionType a").draggable({
        appendTo: "body",
        helper: "clone"
    });

    /*enable drop*/
    $("#divQuestions")
        .droppable({
            drop: function (event, ui) {
                if ($(ui.draggable).hasAttr('qsnType')) {
                    vm.showQsnToAdd($(ui.draggable).attr('qsnType'));
                }
            }
        })
        .sortable();
});