﻿/*view model*/
function ViewModel() {
    var self = this;
    self.questions = ko.observableArray([]);
    
    /*bool: checks if any question at self.question arry*/
    self.hasAnyQuestion = ko.computed(function () {
        var hasQuestion = self.questions().length > 0;
        return hasQuestion;
    });

    self.init = function (object) {
        ko.mapping.fromJS(object.questions, {}, self.questions);
    };
}

$(document).ready(function() {
    var vm = new ViewModel();
    var jsonString = amplify.store("exam");
    var jsonObject = JSON.parse(jsonString);
    vm.init(jsonObject);
    ko.applyBindings(vm);
 
    var questions = vm.questions();
    var hasQuestion = vm.hasAnyQuestion();

});