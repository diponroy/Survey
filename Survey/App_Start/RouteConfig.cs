﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Survey
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                "Default", 
                "", 
                new { controller = "LoginManage", action = "Index" }  // Parameter defaults
            );

            routes.MapRoute(
                name: "DefaultControllerWithActionAndParameter",
                url: "{controller}/{action}/{id}"
            );

            routes.MapRoute(
                name: "DefaultControllerAndAction",
                url: "{controller}/{action}"
            );

            routes.MapRoute(
                name: "DefaultController",
                url: "{controller}"
            );
        }
    }
}